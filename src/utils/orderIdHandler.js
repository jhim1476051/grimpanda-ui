export const orderIdPrefix = 'test-order-id-';

export const encodeOrderId = (orderId) => {
    return orderIdPrefix + orderId;
}

export const decodeOrderId = (orderId) => {
    return orderId.replace(orderIdPrefix, '');
}