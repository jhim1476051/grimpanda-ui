import React, {lazy} from 'react';

import Loadable from 'components/Loadable';
import AdminAuthMainLayout from 'layout/AuthMainLayout/AdminAuthMainLayout';

const AdminPage = Loadable(lazy(() => import('pages/admin/AdminPage')));
const AdminReviewArtworksPage = Loadable(lazy(() => import('pages/admin/AdminReviewArtworksPage')));

const AdminRoutes = {
	path: '/admin',
	element: <AdminAuthMainLayout />,
	children: [
        {
            path: '',
            element: <AdminPage />
        },
        {
            path: 'review-artworks',
            element: <AdminReviewArtworksPage />
        },
	],
};

export default AdminRoutes;
