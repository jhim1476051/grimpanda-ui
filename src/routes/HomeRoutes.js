import React, { lazy } from 'react';

import MainLayout from 'layout/MainLayout';
import Loadable from 'components/Loadable';

const DefaultPage = Loadable(lazy(() => import('pages/DefaultPage')));

const HomeRoutes = {
	path: '/',
	element: <MainLayout />,
	children: [
        {
            path: '',
            element: <DefaultPage />
        },
	]
};

export default HomeRoutes;
