import React, {lazy} from 'react';

import Loadable from 'components/Loadable';
import MainLayout from 'layout/MainLayout';

const ContentsPage = Loadable(lazy(() => import('pages/contents/ContentsPage')));
const ArtworksPage = Loadable(lazy(() => import('pages/contents/ArtworksPage')));
const GoodsPage = Loadable(lazy(() => import('pages/contents/GoodsPage')));

const ContentsRoutes = {
	path: '/contents',
	element: <MainLayout />,
	children: [
        {
            path: '',
            element: <ContentsPage />
        },
        {
            path: 'artworks',
            element: <ArtworksPage />
        },
        {
            path: 'goods',
            element: <GoodsPage />
        },
	],
};

export default ContentsRoutes;
