import React, {lazy} from 'react';

import Loadable from 'components/Loadable';
import CustomerAuthMainLayout from 'layout/AuthMainLayout/CustomerAuthMainLayout';

const MypagePage = Loadable(lazy(() => import('pages/mypage/MypagePage')));
const MyArtworksPage = Loadable(lazy(() => import('pages/mypage/MyArtworksPage')));
const MyAddArtworkPage = Loadable(lazy(() => import('pages/mypage/MyAddArtworkPage')));
const MyOrdersPage = Loadable(lazy(() => import('pages/mypage/MyOrdersPage')));
const MyShoppingBagPage = Loadable(lazy(() => import('pages/mypage/MyShoppingBagPage')));

const MypageRoutes = {
	path: '/mypage',
	element: <CustomerAuthMainLayout />,
	children: [
        {
            path: '',
            element: <MypagePage />
        },
        {
            path: 'artworks',
            element: <MyArtworksPage />
        },
        {
            path: 'add-artwork',
            element: <MyAddArtworkPage />
        },
        {
            path: 'orders',
            element: <MyOrdersPage />
        },
        {
            path: 'shopping-bag',
            element: <MyShoppingBagPage />
        },
	],
};

export default MypageRoutes;
