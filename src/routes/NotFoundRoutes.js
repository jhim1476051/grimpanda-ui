import React from 'react';

import MainLayout from 'layout/MainLayout';
import NotFoundPage from 'pages/NotFoundPage';

const NotFoundRoutes = {
	path: '/',
	element: <MainLayout />,
	children: [
		{
			path: '*',
			element: <NotFoundPage />,
		},
	]
};

export default NotFoundRoutes;
