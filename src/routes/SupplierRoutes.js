import React, {lazy} from 'react';

import Loadable from 'components/Loadable';
import SupplierAuthMainLayout from 'layout/AuthMainLayout/SupplierAuthMainLayout';

const SupplierPage = Loadable(lazy(() => import('pages/supplier/SupplierPage')));
const SupplierItemsPage = Loadable(lazy(() => import('pages/supplier/SupplierItemsPage')));
const SupplierAddItemPage = Loadable(lazy(() => import('pages/supplier/SupplierAddItemPage')));
const SupplierOrdersPage = Loadable(lazy(() => import('pages/supplier/SupplierOrdersPage')));

const SupplierRoutes = {
	path: '/supplier',
	element: <SupplierAuthMainLayout />,
	children: [
        {
            path: '',
            element: <SupplierPage />
        },
        {
            path: 'items',
            element: <SupplierItemsPage />
        },
        {
            path: 'add-item',
            element: <SupplierAddItemPage />  
        },
        {
            path: 'orders',
            element: <SupplierOrdersPage />
        },
	],
};

export default SupplierRoutes;
