import React, {lazy} from 'react';

import Loadable from 'components/Loadable';
import CustomerAuthMainLayout from 'layout/AuthMainLayout/CustomerAuthMainLayout';

const OrderPage = Loadable(lazy(() => import('pages/order/OrderPage')));
const OrderSuccessPage = Loadable(lazy(() => import('pages/order/OrderSuccessPage')));
const OrderFailPage = Loadable(lazy(() => import('pages/order/OrderFailPage')));
const OrderCompletePage = Loadable(lazy(() => import('pages/order/OrderCompletePage')));

const OrderRoutes = {
	path: '/order',
	element: <CustomerAuthMainLayout />,
	children: [
        {
            path: '',
            element: <OrderPage />
        },
        {
            path: 'success',
            element: <OrderSuccessPage />
        },
        {
            path: 'fail',
            element: <OrderFailPage />
        },
        {
            path: 'complete',
            element: <OrderCompletePage />
        },
	],
};

export default OrderRoutes;
