import React from 'react';
import { useRoutes } from 'react-router-dom';

// project import
import LoginRoutes from './LoginRoutes';
import ContentsRoutes from './ContentsRoutes';
import AdminRoutes from './AdminRoutes';
import MypageRoutes from './MypageRoutes';
import NotFoundRoutes from './NotFoundRoutes';
import OrderRoutes from './OrderRoutes';
import HomeRoutes from './HomeRoutes';
import SupplierRoutes from './SupplierRoutes';

// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes() {
	return useRoutes([
		HomeRoutes,
		LoginRoutes, 
		ContentsRoutes, 
		AdminRoutes, 
		MypageRoutes, 
		SupplierRoutes,
		OrderRoutes,
		NotFoundRoutes
	]);
}
