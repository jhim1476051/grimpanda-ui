import {default as axios} from 'utils/axiosHandler';

const ITEMS_URL = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/goods/all';
const ARTWORKS_URL = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/artwork/all';

export const getItems = async (params) => axios.get(ITEMS_URL, params);
export const getArtworks = async (params) => axios.get(ARTWORKS_URL, params);

const SUPPLIER_ITEMS_URL = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/tenants/{tenantId}/goods';

export const getSupplierItems = async (tenantId) => axios.get(SUPPLIER_ITEMS_URL.replace('{tenantId}', tenantId));
export const saveSupplierItem = async (tenantId, body) => axios.postForm(SUPPLIER_ITEMS_URL.replace('{tenantId}', tenantId)+'/save', body);
export const changeSupplierItemStopYn = async (tenantId, body) => axios.put(SUPPLIER_ITEMS_URL.replace('{tenantId}', tenantId), body);

const ARTIST_ARTWORKS_URL = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/tenants/{tenantId}/artwork';

export const getArtistArtworks = async (tenantId) => axios.get(ARTIST_ARTWORKS_URL.replace('{tenantId}', tenantId));
export const saveArtistArtwork = async (tenantId, body) => axios.postForm(ARTIST_ARTWORKS_URL.replace('{tenantId}', tenantId)+'/save', body);
export const changeArtistArtworkStopYn = async (tenantId, body) => axios.put(`${process.env.REACT_APP_API_PATH_PRODUCT}/api/v1/product/update/tenants/${tenantId}/artwork`, body);

const PRODUCT_SHOPPING_BAG_URL = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/tenants/{tenantId}/shoppingbag';

export const addProductToShoppingBag = async (tenantId, body) => axios.post(PRODUCT_SHOPPING_BAG_URL.replace('{tenantId}', tenantId), body);

const CONTENT_API = process.env.REACT_APP_API_PATH_PRODUCT+'/api/v1/product/tenants/{tenantId}/content';

export const uploadContent = async (tenantId, body) => axios.postForm(CONTENT_API.replace('{tenantId}', tenantId), body);
export const deleteContent = async (tenantId, path, body) => axios.post(CONTENT_API.replace('{tenantId}', tenantId) + path, body);
