import {default as axios} from 'utils/axiosHandler';
import qs from 'qs'

export const getShoppingBag = async (tenantId) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/shopping-bag`);
export const addProductToShoppingBag = async (tenantId, body) => axios.post(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/shopping-bag/save`, body);
export const removeProductFromShoppingBag = async (tenantId, path) => axios.delete(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/shopping-bag`+path);

export const getOrderBeforePayment = async (tenantId) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/orderbeforepayment`);
export const saveOrderBeforePayment = async (tenantId, body) => axios.post(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/orderbeforepayment`, body);
export const approveOrderBeforePayment = async (tenantId, body) => axios.post(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/orderbeforepayment/approve`, body);

export const getOrdersAfterPayment = async (tenantId) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/order`);
export const getOrderAfterPayment = async (tenantId, path) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/order`+path);

export const getProductOrders = async (tenantId) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/productorder/all`);
export const getProductOrder = async (tenantId, data) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/productorder`, data);
export const updateProductOrder = async (tenantId, params) => axios.put(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/productorder/save?`+ qs.stringify(params));

export const getDeliveries = async (tenantId, data) => axios.get(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/delivery`, data);
export const updateDelivery = async (tenantId, data) => axios.put(`${process.env.REACT_APP_API_PATH_DELIVERY}/api/v1/delivery/tenants/${tenantId}/delivery`, data);