import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useSnackbar } from 'notistack';

import MainLayout from 'layout/MainLayout/index';

const AdminAuthMainLayout = () => {
    const navigate = useNavigate()
	const memberType = useSelector((state) => {return state.user.memberType});
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        if (memberType !== 'ADMIN') {
            enqueueSnackbar('접근권한이 없습니다.', { variant: 'error' , autoHideDuration : 2000 });
            navigate('/');
        }
    });
    
    return (
        <>
            {memberType === 'ADMIN' ? <MainLayout /> : ''}
        </>
    )
};

export default AdminAuthMainLayout;