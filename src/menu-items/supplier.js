// icons
import {CommentOutlined, BarsOutlined, UserOutlined, SettingOutlined} from '@ant-design/icons';

const icons = {
	CommentOutlined,
	BarsOutlined,
	UserOutlined,
	SettingOutlined,
};

// ==============================|| MENU ITEMS - UTILITIES ||============================== //

const supplier = {
	id: 'supplier-menu',
	title: '공급사 메뉴',
	type: 'group',
	children: [
		{
			// id: '메뉴 id',
			// title: '메뉴 제목',
			// type: 'collapse',
			// icon: 아이콘,
			// children: [
			// 	{
			// 		id: '메뉴 id',
			// 		title: '메뉴 제목',
			// 		type: 'item',
			// 		url: '메뉴랑 매핑할 url',
			// 	},

			id: 'supplier',
			title: 'supplier',
			type: 'collapse',
			icon: icons.UserOutlined,
			children: [
				{
					id: 'supplier-items',
					title: '굿즈',
					type: 'item',
					url: '/supplier/items',
				},
				{
					id: 'supplier-orders',
					title: '주문 발주',
					type: 'item',
					url: '/supplier/orders',
				},
			],
		},
	],
};

export default supplier;
