// icons
import {SettingOutlined} from '@ant-design/icons';

const icons = {
	SettingOutlined,
};

// ==============================|| MENU ITEMS - UTILITIES ||============================== //

const admin = {
	id: 'admin-menu',
	title: '관리자 메뉴',
	type: 'group',
	children: [
		{
			// id: '메뉴 id',
			// title: '메뉴 제목',
			// type: 'collapse',
			// icon: 아이콘,
			// children: [
			// 	{
			// 		id: '메뉴 id',
			// 		title: '메뉴 제목',
			// 		type: 'item',
			// 		url: '메뉴랑 매핑할 url',
			// 	},
			id: 'review-artworks',
			title: '작품 검수',
			type: 'item',
			url: '/admin/review-artworks',
		},
	],
};

export default admin;
