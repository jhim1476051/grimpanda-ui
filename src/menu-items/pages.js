// assets
import { LoginOutlined, ProfileOutlined } from '@ant-design/icons';

// icons
const icons = {
	LoginOutlined,
	ProfileOutlined
};

// ==============================|| MENU ITEMS - EXTRA PAGES ||============================== //

const pages = {
	id: 'authentication',
	title: '인증',
	type: 'group',
	children: [
		{
			id: 'login',
			title: '로그인',
			type: 'item',
			url: '/auth/login',
			icon: icons.LoginOutlined,
		},
		{
			id: 'register',
			title: '회원가입',
			type: 'item',
			url: '/auth/sign-up',
			icon: icons.ProfileOutlined,
		}
	]
};

export default pages;
