// project import
import pages from './pages';
import main from './main';
import mypage from './mypage';
import admin from './admin';
import supplier from './supplier';

// ==============================|| MENU ITEMS ||============================== //

const getMenuItems = (memberType) => {
    const menuItems = {
		items: []
	};
	
	switch (memberType) {
		case 'CUSTOMER' :
			menuItems.items = [main, mypage];
			break
		case 'ADMIN' :
			menuItems.items = [admin];
			break;
		case 'SUPPLIER' :
			menuItems.items = [supplier];
			break;
		default :
			menuItems.items = [pages, main]
			break;
	};

	return menuItems;
}

export default getMenuItems;

