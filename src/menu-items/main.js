// icons
import { BarsOutlined } from '@ant-design/icons';

const icons = {
	BarsOutlined,
};

// ==============================|| MENU ITEMS - UTILITIES ||============================== //

const main = {
	id: 'main-menu',
	title: '메인메뉴',
	type: 'group',
	children: [
		{
			// id: '메뉴 id',
			// title: '메뉴 제목',
			// type: 'collapse',
			// icon: 아이콘,
			// children: [
			// 	{
			// 		id: '메뉴 id',
			// 		title: '메뉴 제목',
			// 		type: 'item',
			// 		url: '메뉴랑 매핑할 url',
			// 	},

			id: 'contents',
			title: 'contents',
			type: 'item',
			icon: icons.BarsOutlined,
			url: '/contents',
		},
	],
};

export default main;
