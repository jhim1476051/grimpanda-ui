// icons
import { UserOutlined } from '@ant-design/icons';

const icons = {
	UserOutlined,
};

// ==============================|| MENU ITEMS - UTILITIES ||============================== //

const mypage = {
	id: 'mypage-menu',
	title: '사용자 메뉴',
	type: 'group',
	children: [
		{
			// id: '메뉴 id',
			// title: '메뉴 제목',
			// type: 'collapse',
			// icon: 아이콘,
			// children: [
			// 	{
			// 		id: '메뉴 id',
			// 		title: '메뉴 제목',
			// 		type: 'item',
			// 		url: '메뉴랑 매핑할 url',
			// 	},


			id: 'mypage',
			title: '마이페이지',
			type: 'item',
			url: '/mypage',
		},
		{
			id: 'mypage-artworks',
			title: '등록 작품',
			type: 'item',
			url: '/mypage/artworks',
		},
		{
			id: 'mypage-orders',
			title: '주문 내역',
			type: 'item',
			url: '/mypage/orders',
		},
		{
			id: 'mypage-shoppingbag',
			title: '장바구니',
			type: 'item',
			url: '/mypage/shopping-bag'
		},
	],
};

export default mypage;
