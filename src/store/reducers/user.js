// types
import {createSlice} from '@reduxjs/toolkit';

// initial state
const initialState = {
	login: '',
	name: '',
	nickname: '',
	email: '',
	hpNo: '',
	memberType: '',
	status: '',
};

// ==============================|| SLICE - MENU ||============================== //

const user = createSlice({
	name: 'user',
	initialState,
	reducers: {
		login(state, action) {
			state.login = action.payload.login;
			state.name = action.payload.name;
			state.nickname = action.payload.nickname;
			state.email = action.payload.email;
			state.hpNo = action.payload.hpNo;
			state.memberType = action.payload.memberType;
			state.status = action.payload.status;
		},

		logout(state) {
			state.login = '';
			state.name = '';
			state.nickname = '';
			state.email = '';
			state.hpNo = '';
			state.memberType = '';
			state.status = '';
		},
	},
});

export default user.reducer;

export const {login, logout} = user.actions;
