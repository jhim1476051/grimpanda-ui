import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const DefaultPage = () => {
	const navigate = useNavigate();
	const memberType = useSelector((state) => {return state.user.memberType});

	useEffect(() => {
		switch (memberType) {
			case 'CUSTOMER' :
				navigate('/mypage');
				break;
			case 'ADMIN' :
				navigate('/admin/review-artworks');
				break;
			case 'SUPPLIER' :
				navigate('/supplier/orders');
				break;
			default :
				navigate('/contents');
				break;
		}
	}, []);

	return <></>;
};

export default DefaultPage;
