import React, { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { Button, Card, CardContent, CardMedia, Grid, Typography, Stack, InputLabel, OutlinedInput } from '@mui/material/index';

import { Formik } from 'formik';
import PropTypes from 'prop-types';
import { loadPaymentWidget } from "@tosspayments/payment-widget-sdk";

import { getOrderBeforePayment, getShoppingBag, saveOrderBeforePayment } from 'api/delivery/delivery';
import { encodeOrderId } from 'utils/orderIdHandler';

const selector = "#payment-widget";
const clientKey = "test_ck_D5GePWvyJnrK0W0k6q8gLzN97Eoq";
const customerKey = "YbX2HuSlsC9uVJW6NMRMj";
const price = 1;

const OrderPage = () => {
	const navigate = useNavigate();
    const user = useSelector((state) => state.user);

	const paymentWidgetRef = useRef(null);
	const paymentMethodsWidgetRef = useRef(null);

	const [shoppingBag, setShoppingBag] = useState([]);
	const [isLoading, setIsLoading] = useState([]);
	const [shippingInfo, setShippingInfo] = useState({
		zipCode: "",
		roadName: "",
		addressDetail: "",
		remark: "",
	});
	const [orderId, setOrderId] = useState(null);

	useEffect(() => {
		findOrderBeforePayment();
		renderPaymentWidget();
	}, []);

	const findOrderBeforePayment = async () => {
		setIsLoading(true);

		await getShoppingBag(user.nickname)
			.then((response) => {
				if(!response) {
					alert("장바구니가 비어 있습니다. 장바구니 화면으로 이동합니다.");
					navigate("/mypage/shopping-bag");
				}
				setShoppingBag(response);
			});

		await getOrderBeforePayment(user.nickname)
			.then((response) => {
				setShippingInfo({
					zipCode: response.shippingInfo.address.zipCode,
					roadName: response.shippingInfo.address.roadName,
					addressDetail: response.shippingInfo.address.detail,
					remark: response.shippingInfo.remark,
				});

				setOrderId(response.id);
			})
			.catch(() => { });

		setIsLoading(false);
	};

	const renderPaymentWidget = async () => {
		const paymentWidget = await loadPaymentWidget(clientKey, customerKey);

		const paymentMethodsWidget = paymentWidget.renderPaymentMethods(selector, { value: price });

		paymentWidgetRef.current = paymentWidget;
		paymentMethodsWidgetRef.current = paymentMethodsWidget;
	};

	const saveShippingInfo = async (values) => {
		return await saveOrderBeforePayment(user.nickname, values)
			.then((response) => {
				setShippingInfo({
					zipCode: response.shippingInfo.address.zipCode,
					roadName: response.shippingInfo.address.roadName,
					addressDetail: response.shippingInfo.address.detail,
					remark: response.shippingInfo.remark,
				});

				setOrderId(response.id);
				return response.id;
			})
			.catch(() => { });
		
	};

	const requestPayment = async (shippingInfo) => {
		const orderId = await saveShippingInfo(shippingInfo);
		
		const paymentWidget = paymentWidgetRef.current;
		const body = {
			orderId: encodeOrderId(orderId),
			orderName: "GrimPanda Order " + orderId,
			customerName: "테스트",
			customerEmail: "테스트",
			successUrl: `${window.location.origin}/order/success`,
			failUrl: `${window.location.origin}/order/fail`,
		};

		try {
			await paymentWidget.requestPayment(body)
		} catch (error) {
			alert(error);
		}
	};

	return (
		<>
			<h1>OrderPage 입니다.</h1>
			<div className="shoppingBag">
				<h1>ShoppingBag</h1>
				<div>
					{
						isLoading
							? "Loading..."
							:
							<>
								<Grid container rowSpacing={2}>
									{shoppingBag.map((product) => <ShoppingBagProduct key={product.id} product={product} />)}
									<Grid item>
										<Typography variant='h5'>
											합계 : {shoppingBag.reduce((previousValue, currentValue, currentIndex, array) => previousValue + currentValue.unitPrice * currentValue.amount, 0)}
										</Typography>
									</Grid>
								</Grid>
							</>
					}
				</div>
			</div>
			<div className="shippingInfo">
				<h1>주문 배송 정보</h1>
				<Formik
					initialValues={{
						zipCode: shippingInfo.zipCode,
						roadName: shippingInfo.roadName,
						addressDetail: shippingInfo.addressDetail,
						remark: shippingInfo.remark,
						submit: null,
					}}
					validate={values => {
						const { zipCode, roadName, addressDetail, remark } = values;
						const errors = {};

						if (!zipCode) errors.zipCode = 'Required';
						if (!roadName) errors.roadName = 'Required'
						if (!addressDetail) errors.roadName = 'Required'

						return errors;
					}}
					onSubmit={(values) => requestPayment(values)}
					enableReinitialize
				>
					{({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting,
					}) => (
						<form noValidate onSubmit={handleSubmit}>
							<Grid container spacing={2}>
								<Grid item xs={2}>
									<Stack spacing={1}>
										<InputLabel required>우편번호</InputLabel>
										<OutlinedInput type='text' value={values.zipCode} name='zipCode' onChange={handleChange} />
									</Stack>
								</Grid>
								<Grid item xs={6}>
									<Stack spacing={1}>
										<InputLabel required>도로명</InputLabel>
										<OutlinedInput type='text' value={values.roadName} name='roadName' onChange={handleChange} />
									</Stack>
								</Grid>
								<Grid item xs={2}>
									<Stack spacing={1}>
										<InputLabel required>상세주소</InputLabel>
										<OutlinedInput type='text' value={values.addressDetail} name='addressDetail' onChange={handleChange} />
									</Stack>
								</Grid>
								<Grid item xs={10}>
									<Stack spacing={1}>
										<InputLabel>비고</InputLabel>
										<OutlinedInput type='text' value={values.remark} name='remark' onChange={handleChange} />
									</Stack>
								</Grid>
								<Grid item xs={12}>
									<div>
										<h1>결제 정보</h1>
										<div id='payment-widget'></div>
									</div>
								</Grid>
								<Grid item xs={12}>
									<Button variant='contained' onClick={handleSubmit}>결제하기</Button>
								</Grid>
							</Grid>
						</form>
					)}
				</Formik>
			</div>


		</>
	);
};

export default OrderPage;

const ShoppingBagProduct = ({ product }) => {
	const { id, artwork, item, unitPrice, amount } = product;

	return (
		<Grid item xs={12}>
			<Grid container spacing={2}>
				<Grid item xs={4}>
					<ShoppingBagProductArtwork artwork={artwork} />
				</Grid>
				<Grid item xs={4}>
					<ShoppingBagProductItem item={item} />
				</Grid>
				<Grid item xs={2}>
					<Typography variant="h5" conponent="div">가격: {unitPrice}원</Typography>
					<Typography variant="h5" conponent="div">수량: {amount}</Typography>
				</Grid>
			</Grid>
		</Grid>
	)
};

ShoppingBagProduct.propTypes = {
	product: PropTypes.shape({
		id: PropTypes.number,
		artwork: PropTypes.object,
		item: PropTypes.object,
		unitPrice: PropTypes.number,
		amount: PropTypes.number,
	}),
	handleRemoveProduct: PropTypes.func,
};

const ShoppingBagProductArtwork = ({ artwork }) => {
	const { title, authorNickname } = artwork;

	return (
		<Card className='artwork'>
			{/* <CardMedia image={artwork.contentPath} /> */}
			<CardContent>
				<Typography variant="h5" conponent="div">{title}</Typography>
				<Typography variant="body" conponent="div">{authorNickname}</Typography>
			</CardContent>
		</Card>
	);
};

ShoppingBagProductArtwork.propTypes = {
	artwork: PropTypes.shape({
		title: PropTypes.string,
		authorNickname: PropTypes.string,
	}),
};

const ShoppingBagProductItem = ({ item }) => {
	const { name, supplierName } = item;

	return (
		<Card className='item'>
			{/* <CardMedia image={item.imagePath} /> */}
			<CardContent>
				<Typography variant="h5" conponent="div">{name}</Typography>
				<Typography variant="body" conponent="div">{supplierName}</Typography>
			</CardContent>
		</Card>
	);
};

ShoppingBagProductItem.propTypes = {
	item: PropTypes.shape({
		name: PropTypes.string,
		supplierName: PropTypes.string,
	}),
};