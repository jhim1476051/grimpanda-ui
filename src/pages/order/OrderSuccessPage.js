import React, { useState, useEffect } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { approveOrderBeforePayment } from 'api/delivery/delivery';
import { decodeOrderId } from 'utils/orderIdHandler';

const OrderSuccessPage = () => {
	const navigate = useNavigate();
	const user = useSelector((state) => state.user);
	const [searchParams] = useSearchParams();

	useEffect(() => {
		requestOrderApprove();
	}, []);

	const requestOrderApprove = async () => {
		const body = {
			orderId: decodeOrderId(searchParams.get("orderId")),
			paymentKey: searchParams.get("paymentKey"),
			tossPaymentOrderId: searchParams.get("orderId"),
			amount: searchParams.get("amount"),
		};

		await approveOrderBeforePayment(user.nickname, body)
			.then((response) => {
				navigate('/order/complete');
			})
			.catch((error) => {
				alert(error);
			});
	}

	return (
		<>
			OrderSuccessPage 입니다.
		</>
	);
};
export default OrderSuccessPage;
