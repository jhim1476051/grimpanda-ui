import React from 'react';
import { useNavigate } from 'react-router-dom';

import { Button, Typography } from '@mui/material';

const OrderCompletePage = () => {
	const navigate = useNavigate();

	return (
		<>
			<Typography variant='h5'>OrderCompletePage 입니다.</Typography>
			<Button variant='contained' onClick={() => navigate('/mypage/orders')}>완료</Button>
		</>
	);
};
export default OrderCompletePage;
