import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import {
	Grid,
	Typography,
	Card,
	CardActionArea,
	CardMedia,
	CardActions,
	CardContent,
	Button,
	Stack,
	Dialog,
	DialogTitle,
	DialogContentText,
	DialogActions,
	DialogContent,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	OutlinedInput,
	FormHelperText,
} from '@mui/material';

import { Formik } from 'formik';
import * as Yup from 'yup';
import { enqueueSnackbar } from 'notistack';
import PropTypes from 'prop-types';

import { changeArtistArtworkStopYn, getArtworks } from 'api/product/product';

const denyTypeCategory = [
	'지적재산권침해', '선정성비속어정치성향', '품질미흡', '차별불평등', '기타'
]

const AdminReviewArtworksPage = () => {
	const admin = useSelector((state) => state.user);

	const [open, setOpen] = useState(false);
	const [artworksToReview, setArtworksToReview] = useState([]);
	const [artworkInReview, setArtworksInReview] = useState({});

	useEffect(() => {
		findArtworksToReview();
	}, []);

	const findArtworksToReview = async () => {
		await getArtworks({ params: { stopYn: 'Y' } })
			.then((response) => {
				setArtworksToReview(response.filter((data) => data.status === "검수중"));
			});
	};

	const handleSubmitApprove = async (data) => {
		const body = {
			id: data.id,
			status: '승인',
			inspector: admin.nickname,
			stopYn: 'N',
		};

		await changeArtistArtworkStopYn(data.author, body)
			.then((response) => {
				findArtworksToReview()
			})
			.catch((error) => {
				enqueueSnackbar('검수 결과 등록 중 오류가 발생했습니다.', {
					variant: 'error',
				});
			});
	};

	const handleClickOnDeny = (data) => {
		setArtworksInReview(data);
		setOpen(true);
	};

	const handleClose = () => {
		setArtworksInReview({});
		setOpen(false);
	};

	return (
		<>
			<Grid container>
				<Grid item xs={12}>
					<Typography variant='h1'>작품 검수</Typography>
				</Grid>
				<Grid item xs={12}>
					<Grid container spacing={2}>
						{artworksToReview.map((item) => (
							<ArtistArtwork
								key={item.id}
								data={item}
								onApprove={handleSubmitApprove}
								onDeny={handleClickOnDeny}
							/>
						))}
					</Grid>
				</Grid>
			</Grid>
			<Dialog open={open} onClose={handleClose}>
				<Formik
					initialValues={{
						denyType: denyTypeCategory[0],
						comment: '',
					}}
					validationSchema={Yup.object().shape({
						denyType: Yup.string().required('거절 사유는 필수입니다.'),
					})}
					onSubmit={async (values, { setErrors, setSubmitting }) => {
						try {
							setSubmitting(true);

							const body = {
								id: artworkInReview.id,
								status: '거절',
								inspector: admin.nickname,
								stopYn: 'Y',
								denyType: values.denyType,
								comment: values.comment,
							};
							
							await changeArtistArtworkStopYn(artworkInReview.author, body)
								.then((response) => {
									handleClose();
									findArtworksToReview();
								})
								.catch((error) => {
									enqueueSnackbar('검수 결과 등록 중 오류가 발생했습니다.', {
										variant: 'error',
									});
								});
						} catch (err) {
							setErrors({ submit: err.message });
						} finally {
							setSubmitting(false);
						}
					}}
				>
					{({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => {
						return (
							<form noValidate onSubmit={handleSubmit}>
								<DialogTitle>작품 거절</DialogTitle>
								<DialogContent>
									<DialogContentText>
										위반 항목 및 거절 사유를 작성해주세요.
									</DialogContentText>
									<Grid container spacing={2}>
										<Grid item xs={12}>
											<Stack spacing={1}>
												<InputLabel
													htmlFor='deny-type'
													required
												>
													{' '}
													거절 사유
												</InputLabel>
												<Select
													id="deny-type-select"
													name="denyType"
													value={values.denyType}
													onChange={handleChange}
												>
													{denyTypeCategory.map((i) =>
														<MenuItem key={i} value={i}>
															{i}
														</MenuItem>
													)}
												</Select>
												{touched.denyType && errors.denyType && (
													<FormHelperText
														error
														id='helper-text-deny-type'
													>
														{errors.denyType}
													</FormHelperText>
												)}
											</Stack>
										</Grid>
										<Grid item xs={12}>
											<Stack spacing={1}>
												<InputLabel
													htmlFor='comment'
												>
													{' '}
													검토 의견
												</InputLabel>
												<OutlinedInput
													fullWidth
													error={Boolean(
														touched.comment && errors.comment,
													)}
													id='comment'
													type='text'
													value={values.comment}
													name='comment'
													onChange={handleChange}
													placeholder='검토 의견을 입력하세요'
												/>
												{touched.comment && errors.comment && (
													<FormHelperText
														error
														id='helper-text-comment'
													>
														{errors.comment}
													</FormHelperText>
												)}
											</Stack>
										</Grid>
									</Grid>
								</DialogContent>
								<DialogActions>
									<Button color='error' onClick={handleClose}>취소</Button>
									<Button onClick={handleSubmit}>확인</Button>
								</DialogActions>
							</form>
						)
					}}
				</Formik>
			</Dialog >
		</>
	);
};
export default AdminReviewArtworksPage;

const ArtistArtwork = ({ data, onApprove, onDeny }) => {
	return (
		<Grid item xs={4}>
			<Card>
				<CardMedia sx={{ height: 200 }} image={data.contentUrl} />
				<CardContent>
					<Grid container>
						<Grid item xs={8}>
							<Typography variant='h5'>제목: {data.title}</Typography>
							<Typography variant='subtitle1'>
								설명: {data.description}
							</Typography>
							<Stack direction='row' spacing={5}>
								<Typography variant='body'>
									가격: {data.price} 원
								</Typography>
								<Typography variant='body'>
									작가: {data.author}
								</Typography>
							</Stack>
						</Grid>
					</Grid>
				</CardContent>
				<CardActions>
					<Grid container justifyContent='flex-end'>
						<Button variant='outlined' onClick={() => onApprove(data)}>승인</Button>
						<Button variant='outlined' color='error' onClick={() => onDeny(data)}>거절</Button>
					</Grid>
				</CardActions>
			</Card>
		</Grid>
	);
};

ArtistArtwork.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		description: PropTypes.string,
		price: PropTypes.number,
		author: PropTypes.string,
		inspector: PropTypes.string,
		contentUrl: PropTypes.string,
		stopYn: PropTypes.string,
		status: PropTypes.string,
		denyType: PropTypes.string,
		comment: PropTypes.string,
	}),
	onApprove: PropTypes.func,
	onDeny: PropTypes.func,
};