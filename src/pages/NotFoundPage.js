import React from 'react';

const NotFoundPage = () => {
	return (
        <>
            경로에 맞는 페이지를 찾을 수 없습니다. 경로가 올바른지 확인해주세요.
        </>
    );
};

export default NotFoundPage;
