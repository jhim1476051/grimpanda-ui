import React, {useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {useSelector} from 'react-redux';

import {
	TextField,
	InputAdornment,
	IconButton,
	Grid,
	Stack,
	OutlinedInput,
	InputLabel,
	FormHelperText,
	Button,
} from '@mui/material';
import {UploadOutlined, CloseOutlined} from '@ant-design/icons';

import {Formik} from 'formik';
import * as Yup from 'yup';
import {enqueueSnackbar} from 'notistack';
import {saveSupplierItem} from 'api/product/product';

const SupplierAddItemPage = () => {
	const navigate = useNavigate();
	const supplier = useSelector((state) => {
		return state.user;
	});

	return (
		<>
			<h1>SupplierAddItemPage 입니다.</h1>
			<Formik
				initialValues={{
					title: '',
					description: '',
					price: '',
					file: '',
				}}
				validationSchema={Yup.object().shape({
					title: Yup.string().required('굿즈명은 필수입니다.'),
					price: Yup.number('가격은 숫자로만 이루어져야 합니다.')
						.required('가격은 필수 입니다.')
						.positive('가격에 적합하지 않습니다.')
						.integer('가격에 적합하지 않습니다.'),
				})}
				onSubmit={async (values, {setErrors, setSubmitting}) => {
					try {
						setSubmitting(true);

						await saveSupplierItem(supplier.nickname, values)
							.then((response) => {
								enqueueSnackbar('굿즈 등록에 성공했습니다.', {
									variant: 'success',
								});
								navigate('/supplier/items');
							})
							.catch((error) => {
								enqueueSnackbar('굿즈 등록에 실패했습니다.', {
									variant: 'error',
								});
							});
					} catch (err) {
						setErrors({submit: err.message});
					} finally {
						setSubmitting(false);
					}
				}}
			>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting,
					setFieldValue,
				}) => {
					return (
						<form noValidate onSubmit={handleSubmit}>
							<Grid container spacing={2}>
								<Grid item xs={6}>
									<Stack spacing={1}>
										<InputLabel
											htmlFor='item-title'
											required
										>
											{' '}
											굿즈명
										</InputLabel>
										<OutlinedInput
											fullWidth
											error={Boolean(
												touched.title && errors.title,
											)}
											id='item-title'
											type='text'
											value={values.title}
											name='title'
											onChange={handleChange}
											placeholder='굿즈명을 입력하세요'
										/>
										{touched.title && errors.title && (
											<FormHelperText
												error
												id='helper-text-item-title'
											>
												{errors.title}
											</FormHelperText>
										)}
									</Stack>
								</Grid>
								<Grid item xs={6}>
									<Stack spacing={1}>
										<InputLabel
											htmlFor='item-price'
											required
										>
											{' '}
											가격
										</InputLabel>
										<OutlinedInput
											fullWidth
											error={Boolean(
												touched.price && errors.price,
											)}
											id='item-price'
											type='text'
											value={values.price}
											name='price'
											onChange={handleChange}
											placeholder='가격을 입력하세요'
										/>
										{touched.price && errors.price && (
											<FormHelperText
												error
												id='helper-text-item-price'
											>
												{errors.price}
											</FormHelperText>
										)}
									</Stack>
								</Grid>
								<Grid item xs={12}>
									<Stack spacing={1}>
										<InputLabel
											htmlFor='item-description'
											required
										>
											{' '}
											굿즈 설명
										</InputLabel>
										<OutlinedInput
											fullWidth
											error={Boolean(
												touched.description &&
													errors.description,
											)}
											id='item-description'
											type='text'
											value={values.description}
											name='description'
											onChange={handleChange}
											placeholder='굿즈 설명을 입력하세요'
										/>
										{touched.description &&
											errors.description && (
												<FormHelperText
													error
													id='helper-text-item-description'
												>
													{errors.description}
												</FormHelperText>
											)}
									</Stack>
								</Grid>
								<Grid item xs={6}>
									<TextField
										type='file'
										InputProps={{
											startAdornment: (
												<InputAdornment position='start'>
													<UploadOutlined />
												</InputAdornment>
											),
										}}
										inputProps={{
											accept: 'image/png, image/jpeg',
										}}
										onChange={(event) => {
											setFieldValue(
												'file',
												event.target.files[0],
											);
										}}
									/>
								</Grid>
								<Grid item xs={6}>
									<Button
										disabled={isSubmitting}
										variant='contained'
										type='submit'
									>
										저장
									</Button>
								</Grid>
							</Grid>
						</form>
					);
				}}
			</Formik>
		</>
	);
};

export default SupplierAddItemPage;
