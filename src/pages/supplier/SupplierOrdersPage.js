import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

import {
	Grid,
	Typography,
	Card,
	CardActionArea,
	CardMedia,
	CardActions,
	CardContent,
	Button,
	Stack,
	Dialog,
	DialogTitle,
	DialogContentText,
	DialogActions,
	DialogContent,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	OutlinedInput,
	FormHelperText,
} from '@mui/material';

import {Formik} from 'formik';
import {enqueueSnackbar} from 'notistack';

import {getProductOrders, updateProductOrder} from 'api/delivery/delivery';
import DataTable from 'components/@extended/DataTable';

const columns = [
	{
		id: 'id',
		label: '주문번호',
		width: 5,
		align: 'left',
	},
	{
		id: 'artwork',
		label: '작품',
		width: 30,
		align: 'left',
	},
	{
		id: 'artist',
		label: '작가',
		width: 10,
		align: 'left',
	},
	{
		id: 'item',
		label: '굿즈',
		width: 30,
		align: 'left',
	},
	{
		id: 'amount',
		label: '수량',
		width: 10,
		align: 'left',
	},
	{
		id: 'status',
		label: '상태',
		width: 10,
		align: 'left',
	},
];

const statusCategory = ['preparation', 'completed', 'canceled'];

const SupplierOrdersPage = () => {
	const supplier = useSelector((state) => state.user);

	const [orders, setOrders] = useState([]);
	const [open, setOpen] = useState(false);
	const [productOrder, setProductOrder] = useState({});

	useEffect(() => {
		findSupplierOrders();
	}, []);

	const findSupplierOrders = async () => {
		await getProductOrders(supplier.nickname).then((response) => {
			const data = response.map((item) => {
				return {
					id: item.poId,
					artwork: item.artwork.title,
					artist: item.artwork.authorNickname,
					item: item.item.name,
					amount: item.amount,
					status: item.status,
				};
			});
			setOrders(data);
		});
	};

	const handleRowClick = (e, row) => {
		if (row.status !== statusCategory[0]) {
			enqueueSnackbar('완료되거나 취소된 주문입니다.', {
				variant: 'error',
			});

			return;
		}
		setProductOrder(row);
		setOpen(true);
	};

	const handleClose = () => {
		setProductOrder({});
		setOpen(false);
	};

	return (
		<>
			<Grid container spacing={2}>
				<Grid item xs={12}>
					<Typography variant='h1'>상품 발주 내역</Typography>
				</Grid>
				<Grid item xs={12}>
					{orders ? (
						// ? <Grid container spacing={1}>{orders.map((item) => <OrderItem key={item.id} data={item}/>)}</Grid>
						<DataTable
							columns={columns}
							rows={orders}
							rowsPerPageOptions={[10, 20, 30]}
							isLoading={false}
							rowClick={handleRowClick}
						/>
					) : (
						''
					)}
				</Grid>
			</Grid>
			<Dialog open={open} onClose={handleClose}>
				<Formik
					initialValues={{
						orderStatus: productOrder.status,
					}}
					onSubmit={async (values, {setErrors, setSubmitting}) => {
						try {
							const data = {
								poId: productOrder.id,
								status: values.orderStatus,
							};

							await updateProductOrder(
								supplier.nickname,
								data,
							).then(() => {
								handleClose();
								findSupplierOrders();
							});
						} catch (err) {
							setErrors({submit: err.message});
						} finally {
							setSubmitting(false);
						}
					}}
				>
					{({
						values,
						errors,
						touched,
						handleChange,
						handleBlur,
						handleSubmit,
						isSubmitting,
					}) => {
						return (
							<form noValidate onSubmit={handleSubmit}>
								<DialogTitle>상품 발주 상태 변경</DialogTitle>
								<DialogContent>
									<Grid container spacing={2}>
										<Grid item xs={12}>
											<Stack spacing={1}>
												<InputLabel
													htmlFor='order-status'
													required
												>
													{' '}
													상태 변경
												</InputLabel>
												<Select
													id='order-status-select'
													name='orderStatus'
													value={values.orderStatus}
													onChange={handleChange}
												>
													{statusCategory.map((i) => (
														<MenuItem
															key={i}
															value={i}
														>
															{i}
														</MenuItem>
													))}
												</Select>
												{touched.orderStatus &&
													errors.orderStatus && (
														<FormHelperText
															error
															id='helper-text-order-status'
														>
															{errors.orderStatus}
														</FormHelperText>
													)}
											</Stack>
										</Grid>
									</Grid>
								</DialogContent>
								<DialogActions>
									<Button color='error' onClick={handleClose}>
										취소
									</Button>
									<Button onClick={handleSubmit}>확인</Button>
								</DialogActions>
							</form>
						);
					}}
				</Formik>
			</Dialog>
		</>
	);
};
export default SupplierOrdersPage;

const SupplierOrderItem = (data) => {
	return <></>;
};

SupplierOrderItem.propTypes = {
	data: PropTypes.shape({
		poId: PropTypes.shape({
			id: PropTypes.number,
			artwork: PropTypes.shape({
				artworkId: PropTypes.number,
				title: PropTypes.string,
				authorNickname: PropTypes.string,
			}),
			item: PropTypes.shape({
				itemId: PropTypes.number,
				name: PropTypes.string,
				supplierName: PropTypes.string,
			}),
			unitPrice: PropTypes.number,
			amount: PropTypes.number,
			status: PropTypes.string,
		}),
	}),
};
