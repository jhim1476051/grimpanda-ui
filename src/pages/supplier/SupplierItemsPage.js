import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { Grid, Typography, Card, CardActionArea, CardMedia, CardActions, CardContent, Button, Stack, Select, MenuItem, Switch } from '@mui/material';
import { changeSupplierItemStopYn, getSupplierItems } from 'api/product/product';

const SupplierItemsPage = () => {
	const navigate = useNavigate();

	const [isLoading, setIsLoading] = useState(false);

	const [items, setItems] = useState([]);
	const supplier = useSelector((state) => { return state.user} );

	useEffect(() => {
		findItems();
	}, []);
	
	const findItems= async () => {
		setIsLoading(true);

		await getSupplierItems(supplier.nickname)
			.then((response) => {
				setItems(response);
			});

		setIsLoading(false);
	};
	
	const changeStopYn = async (data) => {
		const body = {
			id : data.id,
			stopYn : data.stopYn === 'N' ? 'Y' : 'N'
		};

		await changeSupplierItemStopYn(supplier.nickname, body)
			.then((response) => {
				findItems();
			});
	};

	return (
		<>
			<Grid container>
				<Grid container>
					<Grid item xs={12}>
						<Typography variant="h1">굿즈 목록</Typography>
					</Grid>
				</Grid>
				<Grid container justifyContent='flex-end'>
					<Grid item xs={2}>
						<Button variant="contained" onClick={() => navigate("/supplier/add-item")}>굿즈 추가</Button>
					</Grid>
				</Grid>
				<Grid container spacing={2}>
					{
						isLoading
						? ""
						: items.map((item) => <SupplierItem key={item.id} data={item} handleClick={changeStopYn}/>)
					}
				</Grid>
			</Grid>
		</>
	);
};
export default SupplierItemsPage;

const SupplierItem = ({ data, handleClick }) => {
	return (
		<Grid item xs={4}>
			<Card>
					<CardMedia sx={{ height: 200 }} image={data.contentUrl} />
					<CardContent>
						<Grid container>
							<Grid item xs={10}>
								<Typography variant="h5" conponent="div">{data.title}</Typography>
								<Typography variant="subtitle1" conponent="div">{data.description}</Typography>
								<Stack direction="row" spacing={5}>
									<Typography variant="body" conponent="div">가격: {data.price} 원</Typography>
									<Typography variant="body" conponent="div">업체명: {data.seller}</Typography>
								</Stack>
							</Grid>
							<Grid item xs={2}>
								<Switch checked={data && data.stopYn === 'N'} onChange={() => handleClick(data)}/>
							</Grid>
						</Grid>
					</CardContent>
			</Card>
		</Grid>
	)
}

SupplierItem.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		description: PropTypes.string,
		price: PropTypes.number,
		seller: PropTypes.string,
		contentUrl: PropTypes.string,
		stopYn: PropTypes.string,
	}),
	handleClick: PropTypes.func,
}