import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { Grid, Typography, Card, CardMedia, CardActions, CardContent, Button, Stack, Select, MenuItem } from '@mui/material';
import { enqueueSnackbar } from 'notistack';

import { getItems, getArtworks } from 'api/product/product';
import { addProductToShoppingBag } from 'api/delivery/delivery';


const ContentsPage = () => {
	const navigate = useNavigate();
	const user = useSelector((state) => { return state.user });

	const [artworks, setArtworks] = useState([]);
	const [items, setItems] = useState([]);

	const [selectedArtwork, setSelectedArtwork] = useState({});
	const [selectedItem, setSelectedItem] = useState({});

	const [amount, setAmount] = useState(1);

	useEffect(() => {
		findContents();
	}, []);

	const findContents = async () => {
		await getArtworks({ params: { stopYn: 'N' } })
			.then((response) => {
				setArtworks(response);
			});
		await getItems({ params: { stopYn: 'N' } })
			.then((response) => {
				setItems(response);
			});
	}

	const handleArtworkClick = (artwork) => {
		if (selectedArtwork && artwork.id === selectedArtwork.id) {
			setSelectedArtwork({});
		} else {
			setSelectedArtwork(artwork);
		}
	}

	const handleItemClick = (item) => {
		if (selectedItem && item.id === selectedItem.id) {
			setSelectedItem({});
		} else {
			setSelectedItem(item);
		}
	}

	const handleAddProduct = async () => {

		if (user.login !== true) {
			enqueueSnackbar('장바구니에 상품을 담기 위해선 로그인이 필요합니다.', { variant: 'error' });
			navigate('/auth/login');
			return;
		}

		if (!selectedArtwork.id) {
			enqueueSnackbar('작품이 선택되지 않았습니다.', { variant: 'error' });
			return;
		}

		if (!selectedItem.id) {
			enqueueSnackbar('굿즈가 선택되지 않았습니다.', { variant: 'error' });
			return;
		}

		const product = {
			"artwork": {
				"artworkId": selectedArtwork.id,
				"title": selectedArtwork.title,
				"contentPath": selectedArtwork.contentUrl,
				"authorNickname": selectedArtwork.author,
				"artworkAvailableYn": true
			},
			"item": {
				"itemId": selectedItem.id,
				"name": selectedItem.title,
				"imagePath": selectedItem.contentUrl,
				"supplierName": selectedItem.seller,
				"itemAvailableYn": true
			},
			"unitPrice": selectedArtwork.price + selectedItem.price,
			"amount": amount,
		}

		await addProductToShoppingBag(user.nickname, product);

		navigate('/mypage/shopping-bag');
	}

	return (
		<>
			<div>
				<Grid container direction='row' spacing={3}>
					<Grid item xs={6}>
						<Grid item xs={12}>
							<Typography variant='h1'>작품 목록</Typography>
						</Grid>
						<Grid item xs={12} >
							<Grid container spacing={1}>
								{artworks.map((artwork) => <ContentsArtwork
									key={artwork.id}
									data={artwork}
									isSelected={selectedArtwork && selectedArtwork.id === artwork.id}
									handleClick={handleArtworkClick} />
								)}
							</Grid>
						</Grid>
					</Grid>

					<Grid item xs={6}>
						<Grid item xs={12}>
							<Typography variant='h1'>굿즈 목록</Typography>
						</Grid>
						<Grid item xs={12} >
							<Grid container spacing={1}>
								{items.map((item) => <ContentsItem
									key={item.id}
									data={item}
									isSelected={selectedItem && selectedItem.id === item.id}
									handleClick={handleItemClick} />
								)}
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12}>
						<Typography variant='h5'>수량</Typography>
					</Grid>
					<Grid item xs={12}>
						<Stack direction='row' spacing={1}>
							<Select id="product-amount-select" value={amount} label="수량" onChange={(e) => setAmount(e.target.value)} >
								{Array.from({ length: 10 }, (value, index) => index + 1).map((i) => <MenuItem key={i} value={i}>{i}</MenuItem>)}
							</Select>
							<Button variant="contained" onClick={handleAddProduct}>상품 장바구니에 담기</Button>
						</Stack>
					</Grid>
				</Grid>
			</div>
		</>
	);
};

export default ContentsPage;

const ContentsArtwork = ({ data, isSelected, handleClick }) => {
	return (
		<Grid item xs={4}>
			<Card raised={isSelected}>
				<CardMedia sx={{ height: 100 }} image={data.contentUrl} />
				<CardContent>
					<Typography variant="h5" >{data.title}</Typography>
					<Typography variant="subtitle1" >{data.description}</Typography>
					<Stack direction="row" spacing={3}>
						<Typography variant="body" >가격: {data.price} 원</Typography>
						<Typography variant="body" >작가명: {data.author}</Typography>
					</Stack>
				</CardContent>
				<CardActions>
					<Button onClick={() => handleClick(data)}>선택</Button>
				</CardActions>
			</Card>
		</Grid>
	);
};

ContentsArtwork.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		description: PropTypes.string,
		price: PropTypes.number,
		author: PropTypes.string,
		contentUrl: PropTypes.string,
	}),
	isSelected: PropTypes.bool,
	handleClick: PropTypes.func,
}

const ContentsItem = ({ data, isSelected, handleClick }) => {
	return (
		<Grid key={data.id} item xs={4}>
			<Card raised={isSelected}>
				<CardMedia sx={{ height: 100 }} image={data.contentUrl} />
				<CardContent>
					<Typography variant="h5" >{data.title}</Typography>
					<Typography variant="subtitle1" >{data.description}</Typography>
					<Stack direction="row" spacing={3}>
						<Typography variant="body" >가격: {data.price} 원</Typography>
						<Typography variant="body" >업체명: {data.seller}</Typography>
					</Stack>
				</CardContent>
				<CardActions>
					<Button onClick={() => handleClick(data)}>선택</Button>
				</CardActions>
			</Card>
		</Grid>
	)
}

ContentsItem.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		description: PropTypes.string,
		price: PropTypes.number,
		seller: PropTypes.string,
		contentUrl: PropTypes.string,
	}),
	isSelected: PropTypes.bool,
	handleClick: PropTypes.func,
}