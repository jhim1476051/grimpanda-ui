import React from 'react';
import {useNavigate} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import {Grid, Button} from '@mui/material';

import MyArtworksPage from './MyArtworksPage';
import MyOrdersPage from './MyOrdersPage';
import MyShoppingBagPage from './MyShoppingBagPage';

import {activeItem} from 'store/reducers/menu';

const MypagePage = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	return (
		<>
			<Grid container spacing={3}>
				<Grid
					item
					xs={6}
					sx={{
						borderRight: 5,
						borderColor: 'secondary.light',
						paddingBottom: 5,
					}}
				>
					<Button
						sx={{
							width: '100%',
							direction: 'row',
							justifyContent: 'flex-end',
							paddingRight: 5,
						}}
						onClick={() => {
							dispatch(
								activeItem({openItem: ['mypage-artworks']}),
							);
							navigate('/mypage/artworks');
						}}
					>
						더보기
					</Button>
					<MyArtworksPage />
				</Grid>
				<Grid item xs={6}>
					<Button
						sx={{
							width: '100%',
							direction: 'row',
							justifyContent: 'flex-end',
						}}
						onClick={() => {
							dispatch(activeItem({openItem: ['mypage-orders']}));
							navigate('/mypage/orders');
						}}
					>
						더보기
					</Button>
					<MyOrdersPage />
				</Grid>
				<Grid
					item
					xs={12}
					sx={{borderTop: 5, borderColor: 'secondary.light'}}
				>
					<MyShoppingBagPage />
					<Button
						sx={{
							width: '100%',
							direction: 'row',
							justifyContent: 'flex-end',
						}}
						onClick={() => {
							dispatch(
								activeItem({openItem: ['mypage-shoppingbag']}),
							);
							navigate('/mypage/shopping-bag');
						}}
					>
						더보기
					</Button>
				</Grid>
			</Grid>
		</>
	);
};
export default MypagePage;
