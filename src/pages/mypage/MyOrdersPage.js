import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

import {Grid, Typography} from '@mui/material';

import {getOrdersAfterPayment} from 'api/delivery/delivery';
import DataTable from 'components/@extended/DataTable';

const columns = [
	{
		id: 'id',
		label: '주문번호',
		width: 10,
		align: 'left',
	},
	{
		id: 'status',
		label: '상태',
		width: 10,
		align: 'left',
	},
	{
		id: 'artwork',
		label: '작품',
		width: 30,
		align: 'left',
	},
	{
		id: 'item',
		label: '굿즈',
		width: 30,
		align: 'left',
	},
	{
		id: 'amount',
		label: '수량',
		width: 10,
		align: 'left',
	},
];

const MyOrdersPage = () => {
	const user = useSelector((state) => state.user);

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		findMyOrders();
	}, []);

	const findMyOrders = async () => {
		await getOrdersAfterPayment(user.nickname).then((response) => {
			const data = response.reduce((acc, cur, idx, src) => {
				if (cur.products) {
					cur.products.map((item) => {
						acc.push({
							id: cur.id,
							status: item.status,
							artwork: item.artwork.title,
							item: item.item.name,
							amount: item.amount,
						});
					});
				}

				return acc;
			}, []);
			setOrders(data);
		});
	};
	return (
		<>
			<Grid container spacing={2}>
				<Grid item xs={12}>
					<Typography variant='h1'>내 주문 내역</Typography>
				</Grid>
				<Grid item xs={12}>
					{orders ? (
						// ? <Grid container spacing={1}>{orders.map((item) => <OrderItem key={item.id} data={item}/>)}</Grid>
						<DataTable
							columns={columns}
							rows={orders}
							rowsPerPageOptions={[10, 20, 30]}
							isLoading={false}
						/>
					) : (
						''
					)}
				</Grid>
			</Grid>
		</>
	);
};
export default MyOrdersPage;

const OrderItem = (data) => {
	return <></>;
};

OrderItem.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		status: PropTypes.string,
		payment: PropTypes.object,
		shippingInfo: PropTypes.shape({
			address: PropTypes.shape({
				zipCode: PropTypes.string,
				roadName: PropTypes.string,
				detail: PropTypes.string,
			}),
			remark: PropTypes.string,
		}),
		buyer: PropTypes.shape({
			name: PropTypes.string,
			hpNo: PropTypes.string,
			email: PropTypes.string,
		}),
		products: PropTypes.array,
	}),
};
