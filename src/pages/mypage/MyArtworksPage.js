import React, {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

import {
	Grid,
	Typography,
	Card,
	CardActionArea,
	CardMedia,
	CardActions,
	CardContent,
	Button,
	Stack,
	Select,
	MenuItem,
	Switch,
} from '@mui/material';
import {
	changeArtistArtworkStopYn,
	getArtistArtworks,
} from 'api/product/product';

const MyArtworksPage = () => {
	const navigate = useNavigate();

	const [isLoading, setIsLoading] = useState(false);

	const [artworks, setArtworks] = useState([]);
	const supplier = useSelector((state) => {
		return state.user;
	});

	useEffect(() => {
		findArtworks();
	}, []);

	const findArtworks = async () => {
		setIsLoading(true);

		await getArtistArtworks(supplier.nickname).then((response) => {
			setArtworks(response);
		});

		setIsLoading(false);
	};

	const changeStopYn = async (data) => {
		const body = {
			id: data.id,
			stopYn: data.stopYn === 'N' ? 'Y' : 'N',
		};

		await changeArtistArtworkStopYn(supplier.nickname, body)
			.then((response) => {
				findArtworks();
			});
	};

	return (
		<>
			<Grid container>
				<Grid container>
					<Grid item xs={12}>
						<Typography variant='h1'>작품 목록</Typography>
					</Grid>
				</Grid>
				<Grid container justifyContent='flex-end'>
					<Grid item xs={2}>
						<Button
							variant='contained'
							onClick={() => navigate('/mypage/add-artwork')}
						>
							작품 추가
						</Button>
					</Grid>
				</Grid>
				<Grid container spacing={2}>
					{isLoading
						? ''
						: artworks.map((item) => (
								<ArtistArtwork
									key={item.id}
									data={item}
									handleClick={changeStopYn}
								/>
						  ))}
				</Grid>
			</Grid>
		</>
	);
};
export default MyArtworksPage;

const ArtistArtwork = ({data, handleClick}) => {
	return (
		<Grid item xs={4}>
			<Card>
				<CardMedia sx={{height: 200}} image={data.contentUrl} />
				<CardContent>
					<Grid container>
						<Grid item xs={8}>
							<Typography variant='h5'>{data.title}</Typography>
							<Typography variant='subtitle1'>
								{data.description}
							</Typography>
							<Stack direction='row' spacing={5}>
								<Typography variant='body'>
									가격: {data.price} 원
								</Typography>
								<Typography variant='body'>
									작가: {data.author}
								</Typography>
							</Stack>
						</Grid>
						<Grid item xs={4}>
							<Switch
								disabled={data.status !== '승인'}
								checked={data && data.stopYn === 'N'}
								onChange={() => handleClick(data)}
							/>
						</Grid>
					</Grid>
				</CardContent>
				<CardContent>
					<Typography variant='h5'>작품 검수 상세</Typography>
					<Stack direction='row' spacing={5}>
						<Typography variant='body'>
							검수상태: {data.status}
						</Typography>
						<Typography variant='body'>
							검수자 : {data.inspector}
						</Typography>
					</Stack>
					{data.denyType === '거절' ? (
						<Stack spacing={5}>
							<Typography variant='subtitle1'>
								반려사유: {data.denyType}
							</Typography>
							<Typography variant='body'>
								비고: {data.comment}
							</Typography>
						</Stack>
					) : (
						''
					)}
				</CardContent>
			</Card>
		</Grid>
	);
};

ArtistArtwork.propTypes = {
	data: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		description: PropTypes.string,
		price: PropTypes.number,
		author: PropTypes.string,
		inspector: PropTypes.string,
		contentUrl: PropTypes.string,
		stopYn: PropTypes.string,
		status: PropTypes.string,
		denyType: PropTypes.string,
		comment: PropTypes.string,
	}),
	handleClick: PropTypes.func,
};
