import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { Button, Card, CardContent, CardMedia, Grid, Typography } from '@mui/material';

import PropTypes from 'prop-types';

import { getShoppingBag, removeProductFromShoppingBag } from 'api/delivery/delivery';

const MyShoppingBagPage = () => {
    const navigate = useNavigate();
    const user = useSelector((state) => state.user);

    const [shoppingBag, setShoppingBag] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        findShoppingBag();
    }, []);

    const findShoppingBag = async () => {
        setIsLoading(true);

        const response = await getShoppingBag(user.nickname);

        setShoppingBag(response);

        setIsLoading(false);
    }

    const navigateToContentsPage = async () => {
        navigate('/contents');
    }

    const handleRemoveProduct = async (product) => {
        await removeProductFromShoppingBag(user.nickname, "/" + product.id);

        findShoppingBag();
    }

    const navigateToOrderPage = () => {
        navigate('/order');
    }

    return (
        <>
            <Grid container rowSpacing={2}>
                <Grid item xs={2}>
                    <Typography variant='h1'>장바구니</Typography>
                </Grid>
                <Grid item xs={10}>
                    <Button variant='contained' onClick={navigateToContentsPage}>상품 추가하러 가기</Button>
                </Grid>
                <Grid item xs={12}>
                    {
                        isLoading
                            ? "Loading..."
                            :
                            <>
                                <Grid container rowSpacing={2}>
                                    {shoppingBag.map((product) => <ShoppingBagProduct key={product.id} product={product} handleRemoveProduct={handleRemoveProduct} />)}
                                    <Grid item xs={12} >
                                        <Typography variant='h5'>
                                            합계 : {shoppingBag.reduce((previousValue, currentValue, currentIndex, array) => previousValue + currentValue.unitPrice * currentValue.amount, 0)}
                                        </Typography>
                                    </Grid>
                                    {
                                        shoppingBag && shoppingBag.length > 0 ?
                                            <Grid item xs={12} >
                                                <Button variant='contained' onClick={navigateToOrderPage}>주문하기</Button>
                                            </Grid>
                                            : ""
                                    }
                                </Grid>
                            </>
                    }
                </Grid>
            </Grid>
        </>
    );
};
export default MyShoppingBagPage;

const ShoppingBagProduct = ({ product, handleRemoveProduct }) => {
    const { id, artwork, item, unitPrice, amount } = product;

    return (
        <Grid item xs={12}>
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <ShoppingBagProductArtwork artwork={artwork} />
                </Grid>
                <Grid item xs={4}>
                    <ShoppingBagProductItem item={item} />
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="h5" conponent="div">단위가격 : {unitPrice}원</Typography>
                    <Typography variant="h5" conponent="div">수량 : {amount}점</Typography>
                    <Typography variant="h5" conponent="div">총 가격 : {unitPrice * amount}원</Typography>
                </Grid>
                <Grid item xs={2}>
                    <Grid container direction='column'>
                        {/* <Grid item><Button>수정</Button></Grid> */}
                        <Grid item><Button variant="contained" onClick={() => handleRemoveProduct(product)}>삭제</Button></Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
};

ShoppingBagProduct.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number,
        artwork: PropTypes.object,
        item: PropTypes.object,
        unitPrice: PropTypes.number,
        amount: PropTypes.number,
    }),
    handleRemoveProduct: PropTypes.func,
}

const ShoppingBagProductArtwork = ({ artwork }) => {
    const { title, authorNickname } = artwork;

    return (
        <Card className='artwork'>
            {/* <CardMedia image={artwork.contentPath} /> */}
            <CardContent>
                <Typography variant="h5" conponent="div">{title}</Typography>
                <Typography variant="body" conponent="div">{authorNickname}</Typography>
            </CardContent>
        </Card>
    );
};

ShoppingBagProductArtwork.propTypes = {
    artwork: PropTypes.shape({
        title: PropTypes.string,
        authorNickname: PropTypes.string,
    }),
}

const ShoppingBagProductItem = ({ item }) => {
    const { name, supplierName } = item;

    return (
        <Card className='item'>
            {/* <CardMedia image={item.imagePath} /> */}
            <CardContent>
                <Typography variant="h5" conponent="div">{name}</Typography>
                <Typography variant="body" conponent="div">{supplierName}</Typography>
            </CardContent>
        </Card>
    );
}

ShoppingBagProductItem.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        supplierName: PropTypes.string,
    }),
}